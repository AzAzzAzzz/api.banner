<?php

class GetData
{
    /**
     * Результирующий массив
     * @var array
     */
    public $result = [
        'error' => false,
        'message' => null,
        'data' => null
    ];

    private $json;

    // Получение данных в конструкторе
    public function __construct () 
    {
        $this->json = @file_get_contents (dirname(__FILE__) . '/db.json');

        if ( ! $this->json) {
            $this->result['error']   = true;
            $this->result['message'] = 'Сould not retrieve data';
            exit(json_encode($this->result));
        }
    }

    /**
     * Возвращает данные виджета в JSON представлении
     * @return
     */
    public function getWidhetData ()
    {
        $this->result['data'] = json_decode($this->json, true);
        return $this->result;
    }
}

$getData = new GetData();
exit(json_encode($getData->getWidhetData(), true));